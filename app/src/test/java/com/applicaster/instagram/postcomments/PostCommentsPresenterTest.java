/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applicaster.instagram.postcomments;

import com.applicaster.instagram.data.Comment;
import com.applicaster.instagram.data.Post;
import com.applicaster.instagram.data.source.PostsDataSource;
import com.applicaster.instagram.data.source.PostsRepository;
import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for the implementation of {@link PostCommentsPresenterTest}
 */
public class PostCommentsPresenterTest {

    private static List<Comment> COMMENTS;

    private static Post.User newUser = new Post.User("m00gl3","https://scontent.cdninstagram.com/t51.2885-19/s150x150/12331677_189700044711109_254624907_a.jpg");
    private static Comment.From newFrom = new Comment.From("m00gl3","https://scontent.cdninstagram.com/t51.2885-19/s150x150/12331677_189700044711109_254624907_a.jpg");

    private static Post SELECTED_POST = new Post("1590040276411618342_14367861", newUser, new Post.Caption("test 1"), new Post.Comments(1), new Post.Images(new Post.Images.Standard_resolution("https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/21042413_2005564626338600_1836362672413605888_n.jpg")));
    private static Comment SELECTED_COMMENT = new Comment(newFrom, "test");


    @Mock
    private PostsRepository mPostsRepository;

    @Mock
    private PostCommentsContract.View mPostCommentsView;

    /**
     * {@link ArgumentCaptor} is a powerful Mockito API to capture argument values and use them to
     * perform further actions or assertions on them.
     */
    @Captor
    private ArgumentCaptor<PostsDataSource.LoadPostCommentsCallback> mGetPostCommentsCallbackCaptor;

    private PostCommentsPresenter mPostCommentsPresenter;

    @Before
    public void setup() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // The presenter won't update the view unless it's active.
         when(mPostCommentsView.isActive()).thenReturn(true);

        COMMENTS = Lists.newArrayList(SELECTED_COMMENT);
    }

    @Test
    public void createPresenter_setsThePresenterToView() {
        // Get a reference to the class under test
        mPostCommentsPresenter = new PostCommentsPresenter(SELECTED_POST.getId(), mPostsRepository, mPostCommentsView);

        // Then the presenter is set to the view
        verify(mPostCommentsView).setPresenter(mPostCommentsPresenter);
    }

    @Test
    public void getPostCommentsFromRepositoryAndLoadIntoView() {
        // When countries presenter is asked to open a country
        mPostCommentsPresenter = new PostCommentsPresenter(SELECTED_POST.getId(), mPostsRepository, mPostCommentsView);
        mPostCommentsPresenter.start();

        // verify that getCommentsForPost was run (in start), and callback is captured
        verify(mPostsRepository).getCommentsForPost(mGetPostCommentsCallbackCaptor.capture(), eq(SELECTED_POST.getId()));

        // The Mockito captor simulates our callback method
        mGetPostCommentsCallbackCaptor.getValue().onCommentsLoaded(COMMENTS);

        ArgumentCaptor<List> showPostCommentsArgumentCaptor = ArgumentCaptor.forClass(List.class);

        // verifies that showComments was executed with the list that was returned from the callback
        verify(mPostCommentsView).showComments(showPostCommentsArgumentCaptor.capture());

        // Making sure it's 1 as initiated
        assertTrue(showPostCommentsArgumentCaptor.getValue().size() == 1);
    }

}
