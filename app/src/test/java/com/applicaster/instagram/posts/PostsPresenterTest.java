/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applicaster.instagram.posts;


import android.content.Context;
import android.location.Location;

import com.applicaster.instagram.data.location.LocationRepository;
import com.applicaster.instagram.data.location.LocationSource;
import com.google.common.collect.Lists;

import com.applicaster.instagram.data.Post;
import com.applicaster.instagram.data.source.PostsDataSource;
import com.applicaster.instagram.data.source.PostsRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for the implementation of {@link PostsPresenter}
 */
public class PostsPresenterTest {

    private static List<Post> POSTS;

    @Mock
    private PostsRepository mPostsRepository;

    @Mock
    private LocationRepository mLocationRepository;

    @Mock
    private PostsContract.View mPostsView;

    @Mock
    private Context mContext;

    @Mock
    private Location MOCK_LOCATION;

    /**
     * {@link ArgumentCaptor} is a powerful Mockito API to capture argument values and use them to
     * perform further actions or assertions on them.
     */
    @Captor
    private ArgumentCaptor<PostsDataSource.LoadPostsCallback> mLoadPostsCallbackCaptor;

    @Captor
    private ArgumentCaptor<LocationSource.LocationCallback> mStartLocationUpdatesCallbackCaptor;

    private PostsPresenter mPostsPresenter;

    @Before
    public void setupPostsPresenter() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        mPostsPresenter = new PostsPresenter(mPostsRepository, mLocationRepository, mPostsView);

        // The presenter won't update the view unless it's active.
        when(mPostsView.isActive()).thenReturn(true);

        Post.User newUser = new Post.User("m00gl3","https://scontent.cdninstagram.com/t51.2885-19/s150x150/12331677_189700044711109_254624907_a.jpg");

        Post post1 = new Post("1590040276411618342_14367861", newUser, new Post.Caption("test 1"), new Post.Comments(1), new Post.Images(new Post.Images.Standard_resolution("https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/21042413_2005564626338600_1836362672413605888_n.jpg")));
        Post post2 = new Post("1571078786493316324_14367861", newUser, new Post.Caption("test 2"), new Post.Comments(0), new Post.Images(new Post.Images.Standard_resolution("https://scontent.cdninstagram.com/t51.2885-15/sh0.08/e35/p640x640/20482647_262757217545888_4469836279571283968_n.jpg")));
        Post post3 = new Post("1548629929578924004_14367861", newUser, new Post.Caption("test 3"), new Post.Comments(2), new Post.Images(new Post.Images.Standard_resolution("https://scontent.cdninstagram.com/t51.2885-15/e35/19534380_240265496464493_8133223459258892288_n.jpg")));

        POSTS = Lists.newArrayList(post1, post2, post3);

    }


    @Test
    public void createPresenter_setsThePresenterToView() {
        // Get a reference to the class under test
        mPostsPresenter = new PostsPresenter(mPostsRepository, mLocationRepository, mPostsView);

        // Then the presenter is set to the view
        verify(mPostsView).setPresenter(mPostsPresenter);
    }

    @Test
    public void loadAllPostsFromRepositoryAndLoadIntoView() {
        // Given an initialized PostsPresenter with initialized posts
        // When loading of Posts is requested
        mPostsPresenter.initLocationManager(mContext);

        // verify that startWithCallback was run, and Callback is captured and invoked with stubbed location
        verify(mLocationRepository).startWithCallback(mStartLocationUpdatesCallbackCaptor.capture());

        // The Mockito captor simulates our callback method
        mStartLocationUpdatesCallbackCaptor.getValue().locationFound(MOCK_LOCATION);

        // verify that getPostsByLocation was run, and Callback is captured and invoked with stubbed posts
        verify(mPostsRepository).getPostsByLocation(mLoadPostsCallbackCaptor.capture(), eq(MOCK_LOCATION));

        // The Mockito captor simulates our callback method
        mLoadPostsCallbackCaptor.getValue().onPostsLoaded(POSTS);

        ArgumentCaptor<List> showPostsArgumentCaptor = ArgumentCaptor.forClass(List.class);

        // verifies that showPosts was executed with the list that was returned from the callback
        verify(mPostsView).showPosts(showPostsArgumentCaptor.capture());

        // Making sure it's 3 as initiated
        assertTrue(showPostsArgumentCaptor.getValue().size() == 3);
    }


    @Test
    public void unavailablePosts_ShowsError() {
        // Given an initialized PostsPresenter with initialized posts
        // When loading of Posts is requested
        mPostsPresenter.initLocationManager(mContext);

        // verify that startWithCallback was run, and Callback is captured and invoked with stubbed location
        verify(mLocationRepository).startWithCallback(mStartLocationUpdatesCallbackCaptor.capture());

        // The Mockito captor simulates our callback method
        mStartLocationUpdatesCallbackCaptor.getValue().locationFound(MOCK_LOCATION);

        // verify that getPostsByLocation was run, and Callback is captured and invoked with stubbed posts
        verify(mPostsRepository).getPostsByLocation(mLoadPostsCallbackCaptor.capture(), eq(MOCK_LOCATION));

        // The Mockito captor simulates our callback method with 0 posts
        mLoadPostsCallbackCaptor.getValue().onPostsLoaded(new ArrayList<Post>());

        // And the posts aren't available in the repository
        verify(mPostsRepository).getPostsByLocation(mLoadPostsCallbackCaptor.capture(), eq(MOCK_LOCATION));

        mLoadPostsCallbackCaptor.getValue().onDataNotAvailable();

        // Then an error message is shown
        verify(mPostsView).showLoadingPostsError();
    }
}
