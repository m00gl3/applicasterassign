package com.applicaster.instagram.posts;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.applicaster.instagram.Config;
import com.applicaster.instagram.R;

import com.applicaster.instagram.data.Post;
import com.applicaster.instagram.postcomments.PostCommentsActivity;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class PostsFragment extends Fragment implements PostsContract.View {
    private final static String TAG = "PostsFragment - ";

    protected RecyclerView recyclerView;

    protected View mNoPostsView;
    protected ImageView mNoPostIcon;
    protected TextView mNoPostMainView;

    protected LinearLayout linlaHeaderProgress;

    protected PostsContract.Presenter mPresenter;

    protected PostsAdapter adapter;

    private AuthenticationDialog auth_dialog;

    /**
     * Listener for clicks on posts in the ListView.
     */
    protected final PostItemListener mItemListener = new PostItemListener() {
        @Override
        public void onPostClick(Post clickedPost) {
            showPostDetailsUi(clickedPost);
        }
    };

    public static PostsFragment newInstance()
    {
        PostsFragment fragment = new PostsFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.change_radius:
                // Show change radius dialog
                showRadiusDialog();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private void showRadiusDialog()
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        int distance = mPresenter.getDistance();

        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View mView = inflater.inflate(R.layout.radius_picker_dialog, null);
        final SeekBar seekBar = (SeekBar)mView.findViewById(R.id.simpleSeekBar);
        seekBar.setProgress(distance);

        final TextView tvDistanceLabel = (TextView) mView.findViewById(R.id.tvDistanceLabel);
        tvDistanceLabel.setText(String.format(getActivity().getResources().getString(R.string.current_distance), distance));

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            int val = seekBar.getProgress();

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                val = progress + Config.DEFAULT_DISTANCE; //e.g., to convert SeekBar value of 95 to real value of 100

                tvDistanceLabel.setText(String.format(getActivity().getResources().getString(R.string.current_distance), val));
            }

            public void onStartTrackingTouch(SeekBar arg0) {
                // TODO Auto-generated method stub

            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }
        });

        builder.setView(mView)

                // Add action buttons
                .setPositiveButton(getActivity().getResources().getString(R.string.update),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                int progress = seekBar.getProgress() + Config.DEFAULT_DISTANCE;
                                mPresenter.changeDistance(progress);

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                            }
                        });

        // return builder.create();
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {

        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        // This is called in our child classes and is different for each implementation
        adapter = new PostsAdapter(new ArrayList<Post>(0), mItemListener);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.list);

        linlaHeaderProgress = (LinearLayout) rootView.findViewById(R.id.linlaHeaderProgress);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        // Set up  no posts view
        mNoPostsView = rootView.findViewById(R.id.noPosts);
        mNoPostIcon = (ImageView) rootView.findViewById(R.id.noPostsIcon);
        mNoPostMainView = (TextView) rootView.findViewById(R.id.noPostsMain);

        return rootView;
    }

    private void showPostDetailsUi(Post post) {

        Intent intent = new Intent(getContext(), PostCommentsActivity.class);
        intent.putExtra(PostCommentsActivity.EXTRA_POST_ID, post.getId());
        startActivity(intent);
    }

    public void setLoadingIndicator(final boolean active) {

        if (getView() == null) {
            return;
        }

        mNoPostsView.setVisibility(View.GONE);
        linlaHeaderProgress.setVisibility(active ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showNoPosts() {
        adapter.clearData();

        setLoadingIndicator(false);

        showNoPostsViews(
                getResources().getString(R.string.no_posts_all),
                R.drawable.ic_assignment_turned_in_24dp,
                false
        );
    }

    @Override
    public void showPosts(List<Post> posts)
    {
        if (posts != null)
        {
            // Means we're loading more
            if (adapter.getItemCount() > 0)
            {

                adapter.replaceData(posts);

                mNoPostsView.setVisibility(View.GONE);

            }
            else
            {
                adapter.replaceData(posts);
                mNoPostsView.setVisibility(View.GONE);
            }

        }

        setLoadingIndicator(false);
    }


    private void showNoPostsViews(String mainText, int iconRes, boolean showAddView) {
        // mPostsView.setVisibility(View.GONE);
        mNoPostsView.setVisibility(View.VISIBLE);

        mNoPostMainView.setText(mainText);
        mNoPostIcon.setImageDrawable(getResources().getDrawable(iconRes));
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // First we get access token for instagram
        mPresenter.getAccessToken();
    }

    @Override
    public void showAuthenticationDialog()
    {
        auth_dialog = new AuthenticationDialog(getActivity(), mPresenter);
        auth_dialog.setCancelable(true);
        auth_dialog.show();
    }

    @Override
    public void accessTokenGranted()
    {
        // We can now move to the next part of getting the location
        mPresenter.initLocationManager(getActivity());
    }

    @Override
    public void setPresenter(@NonNull PostsContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }



    @Override
    public void showLoadingPostsError()
    {
        setLoadingIndicator(false);
        ((MainActivity)getActivity()).toast(getString(R.string.loading_posts_error));
    }




    @Override
    public boolean isActive() {
        return isAdded();
    }
}