package com.applicaster.instagram.posts;


import com.applicaster.instagram.data.Post;

/**
 * Created by Roy on 8/26/2017.
 */

public interface PostItemListener {

    void onPostClick(Post clickedPost);
}