package com.applicaster.instagram.posts;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.applicaster.instagram.Config;
import com.applicaster.instagram.Injection;
import com.applicaster.instagram.R;

import com.applicaster.instagram.utils.ActivityUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.ErrorDialogFragment;
import com.google.android.gms.common.GoogleApiAvailability;


public class MainActivity extends AppCompatActivity {
    protected static final String TAG = "MainActivity";

    private PostsFragment postsFragment;

    private PostsPresenter mPostsPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (postsFragment == null)
        {
            postsFragment = PostsFragment.newInstance();
        }

        ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                postsFragment, R.id.contentFrame);


        // Presenter Injection
        // Create the presenter
        // Inside the presenter we setPresenter(this) on the view passed (PostsFragment)
        mPostsPresenter = new PostsPresenter(
                Injection.providePostsRepository(getApplicationContext()),
                Injection.provideLocationRepository(this), postsFragment);


    }

    @Override
    protected void onResume()
    {
        super.onResume();

        checkPlayServices();
    }


    private boolean checkPlayServices() {
        // Check that Google Play services is available
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode)
        {
            // In debug mode, log the status
            Log.d("Location Updates", "Google Play services is available.");
            // Continue
            return true;
            // Google Play services was not available for some reason
        }
        else
        {
            // Get the error code
            //int errorCode = connectionResult.getErrorCode();

            // Get the error dialog from Google Play services
            Dialog errorDialog = GoogleApiAvailability.getInstance().getErrorDialog(
                    this,
                    resultCode,
                    Config.CONNECTION_FAILURE_RESOLUTION_REQUEST);

            // If Google Play services can provide an error dialog
            if (errorDialog != null) {
                // Create a new DialogFragment for the error dialog
                ErrorDialogFragment errorFragment = ErrorDialogFragment.newInstance(errorDialog);

                errorDialog.setOnDismissListener(new DialogInterface.OnDismissListener()
                {

                    @Override
                    public void onDismiss(DialogInterface arg0) {
                        finish();
                    }

                });

                // Show the error dialog in the DialogFragment
                errorFragment.show(
                        getFragmentManager(),
                        "Location Updates");
            }

            return false;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Config.MY_PERMISSIONS_REQUEST_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!
                    mPostsPresenter.initLocationManager(this);

                } else {

                    finish();
                }
                return;
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPostsPresenter.stopLocationManager();
    }

    @Override
    public void onPause()
    {
        super.onPause();

        mPostsPresenter.pauseLocationManager();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


	        /*
	         * Handle results returned to the FragmentActivity
	         * by Google Play services
	         */
        // Decide what to do based on the original request code
        switch (requestCode) {
            case Config.REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");

                        mPostsPresenter.initLocationManager(this);
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        finish();
                        break;
                }
                break;
            case Config.CONNECTION_FAILURE_RESOLUTION_REQUEST:
            {
	            /*
	             * If the result code is Activity.RESULT_OK, try
	             * to connect again
	             */
                switch (resultCode) {
                    case Activity.RESULT_OK :
	                    /*
	                     * Try the request again
	                     */

                        break;
                }
                break;
            }
            case Config.REQUEST_CODE_RECOVER_PLAY_SERVICES:
            {
                if (resultCode == RESULT_CANCELED) {
                    toast("Google Play Services must be installed.");
                    // finish();
                }

                break;
            }
        }
    }

    public void toast(String msg)
    {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


}
