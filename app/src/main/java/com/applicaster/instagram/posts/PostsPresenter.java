package com.applicaster.instagram.posts;

/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;


import com.applicaster.instagram.data.Post;
import com.applicaster.instagram.data.PostsResponse;

import com.applicaster.instagram.data.location.LocationRepository;
import com.applicaster.instagram.data.location.LocationSource;
import com.applicaster.instagram.data.source.PostsDataSource;
import com.applicaster.instagram.data.source.PostsRepository;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listens to user actions from the UI ({@link PostsFragment}), retrieves the data and updates the
 * UI as required.
 */
public class PostsPresenter implements PostsContract.Presenter {

    private final PostsRepository mPostsRepository;
    private final LocationRepository mLocationRepository;

    private final PostsContract.View mPostsView;

    private boolean mFirstLoad = true;

    private final PostsDataSource.AccessTokenCallback accessTokenCallback = new PostsDataSource.AccessTokenCallback()
    {
        @Override
        public void accessGranted()
        {
            mPostsView.accessTokenGranted();
        }

        @Override
        public void showAuthDialog()
        {
            // Once we're done here, we'll save the access token and continue on our merry way
            mPostsView.showAuthenticationDialog();
        }
    };

    private final PostsDataSource.LoadPostsCallback loadPostsCallback = new PostsDataSource.LoadPostsCallback()
    {
        @Override
        public void onResponse(Call<PostsResponse> call, Response<PostsResponse> response) {}

        @Override
        public void onFailure(Call<PostsResponse> call, Throwable t) {}

        @Override
        public void onPostsLoaded(List<Post> posts)
        {
            if (posts == null)
            {
                mPostsView.showNoPosts();
            }
            else
            {
                // The view may not be able to handle UI updates anymore
                if (!mPostsView.isActive()) {
                    return;
                }

                processPosts(posts);
            }
        }

        @Override
        public void onDataNotAvailable()
        {

            // The view may not be able to handle UI updates anymore
            if (!mPostsView.isActive()) {
                return;
            }

            mPostsView.showLoadingPostsError();
        }

    };

    private final LocationSource.LocationCallback locationCallback = new LocationSource.LocationCallback()
    {
        @Override
        public void locationFound(Location location)
        {
            // We can load our posts once we have a location
            loadPosts(true, location);
        }

        @Override
        public void connected()
        {

        }
    };

    public PostsPresenter(@NonNull PostsRepository postsRepository, @NonNull LocationRepository locationRepository, @NonNull PostsContract.View postsView) {
        mPostsRepository = checkNotNull(postsRepository, "PostsRepository cannot be null");
        mLocationRepository = checkNotNull(locationRepository, "LocationRepository cannot be null");

        mPostsView = checkNotNull(postsView, "PostsView cannot be null!");

        mPostsView.setPresenter(this);
    }

    @Override
    public void getAccessToken()
    {
        mPostsRepository.getAccessToken(accessTokenCallback);
    }

    @Override
    public void setAccessToken(String accessToken)
    {
        mPostsRepository.setAccessToken(accessToken);

        // This will start our next step in the process
        mPostsView.accessTokenGranted();
    }

    @Override
    public void changeDistance(int newDistance)
    {
        mPostsRepository.setDistance(newDistance);

        Location location = mLocationRepository.getLocation();

        if (location != null)
            loadPosts(true, location);
    }

    @Override
    public int getDistance()
    {
        return mPostsRepository.getDistance();
    }

    // This is only called if we didn't have permissions,
    // we are coming back from activity request permission result
    @Override
    public void startLocationUpdates()
    {
        mLocationRepository.startLocationUpdates();
    }

    @Override
    public void start()
    {

    }

    // This method is called when we have permission for location
    // and that location is enabled
    @Override
    public void initLocationManager(Context context)
    {
        mLocationRepository.startWithCallback(locationCallback);
    }

    @Override
    public void pauseLocationManager()
    {
        mLocationRepository.pause();
    }

    @Override
    public void stopLocationManager()
    {
        mLocationRepository.stop();
    }

    /**
     * @param showLoadingUI Pass in true to display a loading icon in the UI
     */
    public void loadPosts(final boolean showLoadingUI, Location location) {
        if (showLoadingUI) {
            mPostsView.setLoadingIndicator(true);
        }

        mPostsRepository.getPostsByLocation(loadPostsCallback, location);
    }


    private void processPosts(List<Post> posts) {
        if (posts.isEmpty()) {
            // Show a message indicating there are no PostsContract for that filter type.
            processEmptyPosts();
        } else {
            // Show the list of PostsContract
            mPostsView.showPosts(posts);
        }
    }

    private void processEmptyPosts() {
        mPostsView.showNoPosts();
    }


}
