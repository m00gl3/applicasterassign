package com.applicaster.instagram.posts;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.applicaster.instagram.R;
import com.applicaster.instagram.data.Post;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roy on 9/24/2017.
 */

public class PostsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Post> mPosts;
    private PostItemListener mItemListener;

    public PostsAdapter(List<Post> countries, PostItemListener itemListener) {
        setList(countries);
        mItemListener = itemListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        return new PostHolder(inflater.inflate(R.layout.row_post,parent,false), mItemListener);

    }

    public void clearData()
    {
        mPosts.clear();
        notifyDataSetChanged();
    }

    public void replaceData(List<Post> countries) {
        setList(countries);
        notifyDataSetChanged();
    }


    protected void setList(List<Post> countries) {
        mPosts = checkNotNull(countries);
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ((PostHolder)holder).bindData(mPosts.get(position));

        //No else part needed as load holder doesn't bind any data
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }


    /* VIEW HOLDER */
    public static class PostHolder extends RecyclerView.ViewHolder {
        TextView tv_user_fullname;
        TextView tv_user_caption;
        TextView tv_comments;

        ImageView iv_photo;
        ImageView iv_profile;

        View rowView;

        Context mContext;

        private PostItemListener itemListener;

        public PostHolder(View itemView, PostItemListener postItemListener) {
            super(itemView);

            mContext = itemView.getContext();

            itemListener = postItemListener;
            rowView = itemView;

            tv_user_fullname = (TextView) rowView.findViewById(R.id.tv_user_fullname);
            tv_user_caption = (TextView) rowView.findViewById(R.id.tv_user_caption);
            tv_comments = (TextView) rowView.findViewById(R.id.tv_comments);

            iv_photo = (ImageView) rowView.findViewById(R.id.iv_photo);
            iv_profile = (ImageView) rowView.findViewById(R.id.iv_profile);

        }

        void bindData(final Post post)
        {
            tv_user_fullname.setText(post.getUser().getUserName());
            tv_user_caption.setText(post.getCaption() != null ? post.getCaption().getText() : "");

            if (post.getComments().getCount() > 0)
            {
                tv_comments.setVisibility(View.VISIBLE);

                if (post.getComments().getCount() == 1)
                {
                    tv_comments.setText(mContext.getResources().getString(R.string.view_one_comment));
                }
                else
                {
                    String viewAllComments = String.format(mContext.getResources().getString(R.string.view_all_comment), post.getComments().getCount());
                    tv_comments.setText(viewAllComments);
                }

                rowView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        itemListener.onPostClick(post);
                    }
                });
            }
            else
            {
                tv_comments.setVisibility(View.GONE);
            }

            Picasso.with(mContext)
                    .load(post.getUser().getProfile_picture())
                    .resize(100, 100)
                    .centerInside()
                    .into(iv_profile);

            Picasso.with(mContext)
                    .load(post.getImages().getStandard_resolution().getUrl())
                    .into(iv_photo);

        }
    }

}
