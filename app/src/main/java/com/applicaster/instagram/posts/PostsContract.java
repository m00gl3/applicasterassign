package com.applicaster.instagram.posts;

/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;


import com.applicaster.instagram.BasePresenter;
import com.applicaster.instagram.BaseView;
import com.applicaster.instagram.data.Post;

import java.util.List;

/**
 * This specifies the contract between the view and the presenter.
 */
public interface PostsContract {

    // The interface between the PRESENTER and the VIEW
    interface View extends BaseView<Presenter> {
        void showLoadingPostsError();

        void showNoPosts();

        void setLoadingIndicator(boolean active);

        void showPosts(List<Post> posts);

        boolean isActive();

        void accessTokenGranted();

        void showAuthenticationDialog();

    }

    // The interface between the VIEW and the PRESENTER
    interface Presenter extends BasePresenter
    {
        void getAccessToken();

        void setAccessToken(String accessToken);

        void startLocationUpdates();

        void initLocationManager(Context context);

        void pauseLocationManager();

        void stopLocationManager();

        void changeDistance(int newDistance);

        int getDistance();
    }
}
