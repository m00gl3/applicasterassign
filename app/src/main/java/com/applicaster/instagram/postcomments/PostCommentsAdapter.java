package com.applicaster.instagram.postcomments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.applicaster.instagram.R;
import com.applicaster.instagram.data.Comment;
import com.applicaster.instagram.data.Post;
import com.applicaster.instagram.posts.PostsAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Roy on 9/12/2017.
 */

public class PostCommentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Comment> mComments;
    
    public PostCommentsAdapter(List<Comment> comments) {
        setList(comments);

    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        return new CommentHolder(inflater.inflate(R.layout.row_comment,parent,false));

    }

    public void clearData()
    {
        mComments.clear();
        notifyDataSetChanged();
    }

    public void replaceData(List<Comment> comments) {
        setList(comments);
        notifyDataSetChanged();
    }


    private void setList(List<Comment> comments) {
        mComments = checkNotNull(comments);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ((PostCommentsAdapter.CommentHolder)holder).bindData(mComments.get(position));

    }

    @Override
    public int getItemCount() {
        return mComments.size();
    }


    /* VIEW HOLDER */
    public static class CommentHolder extends RecyclerView.ViewHolder {
        TextView tv_username;
        TextView tv_comment;

        ImageView iv_comment_profile;

        View rowView;

        Context mContext;


        public CommentHolder(View itemView) {
            super(itemView);

            mContext = itemView.getContext();

            rowView = itemView;

            tv_username = (TextView) rowView.findViewById(R.id.tv_username);
            tv_comment = (TextView) rowView.findViewById(R.id.tv_comment);

            iv_comment_profile = (ImageView) rowView.findViewById(R.id.iv_comment_profile);

        }

        void bindData(final Comment comment)
        {
            tv_username.setText(comment.getFrom().getUserName());
            tv_comment.setText(comment.getText());

            Picasso.with(mContext)
                    .load(comment.getFrom().getProfile_picture())
                    .resize(100, 100)
                    .centerInside()
                    .into(iv_comment_profile);


        }
    }
}
