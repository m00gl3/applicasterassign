/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applicaster.instagram.postcomments;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.applicaster.instagram.Injection;
import com.applicaster.instagram.R;
import com.applicaster.instagram.data.Post;
import com.applicaster.instagram.utils.ActivityUtils;


/**
 * Displays post details screen.
 */
public class PostCommentsActivity extends AppCompatActivity {

    public static final String EXTRA_POST_ID = "POST_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.postcomments_act);

        // Set up the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowHomeEnabled(true);

        // Get the requested post id
        String postId = getIntent().getStringExtra(EXTRA_POST_ID);

        String title = getResources().getString(R.string.comments);
        ab.setTitle(title);

        PostCommentsFragment postCommentsFragment = (PostCommentsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);

        if (postCommentsFragment == null) {
            postCommentsFragment = PostCommentsFragment.newInstance();

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    postCommentsFragment, R.id.contentFrame);
        }

        // Create the presenter
        new PostCommentsPresenter(postId, Injection.providePostsRepository(getApplicationContext()), postCommentsFragment);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void toast(String msg)
    {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
