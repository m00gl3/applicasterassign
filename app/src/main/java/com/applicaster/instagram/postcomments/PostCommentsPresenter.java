/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applicaster.instagram.postcomments;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.applicaster.instagram.data.Comment;
import com.applicaster.instagram.data.CommentsResponse;
import com.applicaster.instagram.data.Post;
import com.applicaster.instagram.data.source.PostsDataSource;
import com.applicaster.instagram.data.source.PostsRepository;


import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listens to user actions from the UI ({@link PostCommentsFragment}), retrieves the data and updates
 * the UI as required.
 */
public class PostCommentsPresenter implements PostCommentsContract.Presenter {

    private final PostsRepository mPostsRepository;

    private final PostCommentsContract.View mPostCommentView;

    private PostsDataSource.LoadPostCommentsCallback loadCommentsCallback = new PostsDataSource.LoadPostCommentsCallback() {

        @Override
        public void onCommentsLoaded(List< Comment > comments)
        {
            mPostCommentView.showComments(comments);
        }

    };

    @Nullable
    private String mPostId;

    public PostCommentsPresenter(@Nullable String postId,
                                 @NonNull PostsRepository postsRepository,
                                 @NonNull PostCommentsContract.View postDetailView) {

        mPostId = postId;
        mPostsRepository = checkNotNull(postsRepository, "postsRepository cannot be null!");
        mPostCommentView = checkNotNull(postDetailView, "countryDetailView cannot be null!");

        mPostCommentView.setPresenter(this);
    }



    @Override
    public void start() {
        loadComments(mPostId);
    }

    public void loadComments(String postId)
    {
        mPostsRepository.getCommentsForPost(loadCommentsCallback, postId );
    }


}
