package com.applicaster.instagram.data;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Roy on 9/24/2017.
 */

public class CommentsResponse
{
    public List<Comment> getComments() {
        return comments;
    }

    @SerializedName("data")
    private List<Comment> comments;

    public CommentsResponse(List<Comment> comments)
    {
        this.comments = comments;
    }
}
