package com.applicaster.instagram.data.source;

/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;

import com.applicaster.instagram.Config;
import com.applicaster.instagram.data.Comment;
import com.applicaster.instagram.data.CommentsResponse;
import com.applicaster.instagram.data.Post;
import com.applicaster.instagram.data.PostsResponse;

import com.applicaster.instagram.data.location.FusedLocationSource;
import com.applicaster.instagram.utils.InstagramSession;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Concrete implementation to load Posts from the data sources into a cache.
 */
public class PostsRepository implements PostsDataSource {

    private static PostsRepository INSTANCE = null;

    private final PostsDataSource mPostsRemoteDataSource;

    private InstagramSession mSession;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    Map<String, Post> mCachedPosts;

    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */
    boolean mCacheIsDirty = false;

    private String mAccessToken;

    public int getDistance() {
        return mDistance;
    }

    private int mDistance = Config.DEFAULT_DISTANCE;

    // Prevent direct instantiation.
    private PostsRepository(@NonNull PostsDataSource PostsRemoteDataSource, InstagramSession instSession) {
        mPostsRemoteDataSource = checkNotNull(PostsRemoteDataSource);

        mSession = instSession;
        mAccessToken = mSession.getAccessToken();
    }

    /**
     * Returns the single instance of this class, creating it if necessary.
     *
     * @param postsRemoteDataSource the backend data source
     * @return the {@link PostsRepository} instance
     */
    public static PostsRepository getInstance(PostsDataSource postsRemoteDataSource, InstagramSession instSession) {
        if (INSTANCE == null) {
            INSTANCE = new PostsRepository(postsRemoteDataSource, instSession);
        }
        return INSTANCE;
    }

    /**
     * Used to force {@link #getInstance(PostsDataSource, InstagramSession)} to create a new instance
     * next time it's called.
     */
    public static void destroyInstance() {
        INSTANCE = null;
    }



    @Override
    public void getAccessToken(@NonNull final AccessTokenCallback callback)
    {
        if (mAccessToken != null)
        {
            callback.accessGranted();
        }
        else
        {
            callback.showAuthDialog();
        }

    }

    @Override
    public void setAccessToken(String accessToken)
    {
        mSession.storeAccessToken(accessToken);
    }

    @Override
    public void getPostComments(@NonNull final LoadCommentsCallback callback, @NonNull String postId, @NonNull String accessToken)
    {
        // Not defined
    }

    // We'll run this for every media item that we bring back
    // to get the "user comment" part
    @Override
    public void getPostComments(@NonNull String postId, @NonNull final LoadCommentsCallback callback) {
        checkNotNull(callback);

        getPostCommentsFromRemoteDataSource(postId, callback );
    }

    private void getPostCommentsFromRemoteDataSource(@NonNull String postId, @NonNull final LoadCommentsCallback callback ) {

        mPostsRemoteDataSource.getPostComments(new LoadCommentsCallback()
        {
            @Override
            public void onResponse(Call<CommentsResponse> call, Response<CommentsResponse> response) {
                // response.isSuccessful() is true if the response code is 2xx
                CommentsResponse commentsResponse = response.body();

                List<Comment> comments = commentsResponse.getComments();

                if (comments == null)
                {
                    onDataNotAvailable();
                }
                else
                {
                    onCommentsLoaded(comments);
                }

            }

            @Override
            public void onFailure(Call<CommentsResponse> call, Throwable t) {
                // handle execution failures like no internet connectivity

                onDataNotAvailable();
            }

            @Override
            public void onCommentsLoaded(List<Comment> comments)
            {
              //  refreshCache(posts, true);

                callback.onCommentsLoaded(comments);
            }

            @Override
            public void onDataNotAvailable()
            {
                callback.onDataNotAvailable();
            }

        }, postId, mAccessToken);
    }

    /**
     * Gets Posts from cache, local data source (SQLite) or remote data source, whichever is
     * available first.
     * <p>
     * Note: {@link LoadPostsCallback#onDataNotAvailable()} is fired if all data sources fail to
     * get the data.
     */
    @Override
    public void getPostsByLocation(@NonNull final LoadPostsCallback callback, @NonNull Location userLocation) {
        checkNotNull(callback);

        getPostsFromRemoteDataSource(callback, userLocation);
    }

    @Override
    public void getPostsByLocation(@NonNull final LoadPostsCallback callback, @NonNull Location userLocation, @NonNull Integer distance, @NonNull String accessToken) {
        // Not Defined
    }

    private void getPostsFromRemoteDataSource(@NonNull final LoadPostsCallback callback, @NonNull final Location userLocation) {

        if (mAccessToken == null || mAccessToken == "")
            mAccessToken = mSession.getAccessToken();

        mPostsRemoteDataSource.getPostsByLocation(new LoadPostsCallback()
        {
            @Override
            public void onResponse(Call<PostsResponse> call, Response<PostsResponse> response) {
                // response.isSuccessful() is true if the response code is 2xx
                PostsResponse postsResponse = response.body();

                if (postsResponse == null)
                {
                    onDataNotAvailable();
                }
                else
                {
                    List<Post> posts = postsResponse.getPosts();

                    if (posts == null)
                    {
                        onDataNotAvailable();
                    }
                    else
                    {
                        onPostsLoaded(posts);
                    }
                }


            }

            @Override
            public void onFailure(Call<PostsResponse> call, Throwable t) {
                // handle execution failures like no internet connectivity

                onDataNotAvailable();
            }

            @Override
            public void onPostsLoaded(List<Post> posts)
            {
                // We'll expand our search radius incrementally until we either have a minimum
                // of 10 posts or we reached Instagram API's max distance allowed
                if (posts.isEmpty() && mDistance < Config.MAX_DISTANCE)
                {
                    mDistance += Config.INCREMENT_STEP_METERS;

                    if (mDistance > Config.MAX_DISTANCE)
                    {
                        mDistance = Config.MAX_DISTANCE;
                    }

                    getPostsFromRemoteDataSource(callback, userLocation);
                }
                else
                {
                    // Limits the results shown to our set limit
                    if (posts.size() > Config.POSTS_LIMIT)
                    {
                        posts = posts.subList(0, Config.POSTS_LIMIT);
                    }

                    // This clears our data with the new posts
                    refreshCache(posts, true);

                    // We'll now retrieve the comments for each post
                    updatePostsWithUserComment(callback);
                }
            }

            @Override
            public void onDataNotAvailable()
            {
                callback.onDataNotAvailable();
            }

        }, userLocation, mDistance, mAccessToken);
    }

    @Override
    public void setDistance(int newDistance)
    {
        mDistance = newDistance;
    }

    private void updatePostsWithUserComment(@NonNull final LoadPostsCallback callback)
    {
        checkNotNull(callback);

        if (mCachedPosts == null ) {
            callback.onDataNotAvailable();
        }
        else if (mCachedPosts.isEmpty())
        {
            callback.onPostsLoaded(new ArrayList<Post>());
        }
        else
        {
            // For each of our posts we'll retrieve the comment and update it
            final List<Post> postsToShow = new ArrayList<Post>();

            final Iterator<Map.Entry<String, Post>> it = mCachedPosts.entrySet().iterator();
            while (it.hasNext())
            {
                final Map.Entry<String, Post> entry = it.next();

                if (entry.getValue().getComments().getCount() > 0)
                {
                    getPostComments(entry.getValue().getId(), new LoadCommentsCallback() {
                        @Override
                        public void onResponse(Call<CommentsResponse> call, Response<CommentsResponse> response) {

                        }

                        @Override
                        public void onFailure(Call<CommentsResponse> call, Throwable t) {

                        }

                        @Override
                        public void onCommentsLoaded(List<Comment> comments)
                        {
                            // Get the first comment,
                            // compare it to user id

                            Post newPost = entry.getValue();

                            if (comments != null && !comments.isEmpty())
                                newPost.getPostComments().addAll(comments);

                            postsToShow.add(entry.getValue());

                            // Last one. Because all of this is done asynchronusly
                            // I need to call the callback from inside the comments callback
                            if (!it.hasNext())
                            {
                                callback.onPostsLoaded(postsToShow);
                            }
                        }

                        @Override
                        public void onDataNotAvailable() {
                            callback.onDataNotAvailable();
                        }
                    });
                }
                else
                {
                    postsToShow.add(entry.getValue());

                    if (!it.hasNext())
                    {
                        callback.onPostsLoaded(postsToShow);
                    }
                }
            }
        }
    }

    @Override
    public void getCommentsForPost(@NonNull LoadPostCommentsCallback callback, @NonNull String postId)
    {
        checkNotNull(callback);

        callback.onCommentsLoaded(mCachedPosts.get(postId).getPostComments());

    }

    private void refreshCache(List<Post> posts, boolean shouldClear) {
        if (mCachedPosts == null) {
            mCachedPosts = new LinkedHashMap<>();
        }

        if (shouldClear)
            mCachedPosts.clear();

        for (Post post : posts) {
            mCachedPosts.put(post.getId(), post);
        }
        mCacheIsDirty = false;
    }


}