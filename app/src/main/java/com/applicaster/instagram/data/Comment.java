package com.applicaster.instagram.data;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Roy on 9/25/2017.
 */

public class Comment
{

    @SerializedName("from")
    private From from;

    public String getText() {
        return text;
    }

    @SerializedName("text")
    private String text;

    public From getFrom() {
        return from;
    }

    public Comment(From from, String text)
    {
        this.from = from;
        this.text = text;
    }

    static public class From
    {
        @SerializedName("username")
        private String username;

        @SerializedName("profile_picture")
        private String profile_picture;

        public String getProfile_picture() {
            return profile_picture;
        }

        public String getUserName() {
            return username;
        }

        public From(@Nullable String username, @Nullable String profile_picture)
        {
            this.username = username;
            this.profile_picture = profile_picture;
        }
    }

}
