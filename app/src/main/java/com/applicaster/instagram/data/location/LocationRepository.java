package com.applicaster.instagram.data.location;

/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.applicaster.instagram.data.Post;
import com.applicaster.instagram.data.PostsResponse;
import com.applicaster.instagram.data.source.PostsDataSource;
import com.google.android.gms.location.LocationListener;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;


public class LocationRepository implements LocationSource {

    private static LocationRepository INSTANCE = null;

    private LocationSource mFusedLocationSource;

    private Location mLocation;

    boolean mLocationIsDirty = false;

    // Prevent direct instantiation.
    private LocationRepository(@NonNull LocationSource fusedLocationSource) {
        mFusedLocationSource = checkNotNull(fusedLocationSource);

    }

    /**
     * Returns the single instance of this class, creating it if necessary.
     *

     * @return the {@link LocationRepository} instance
     */
    public static LocationRepository getInstance(LocationSource fusedLocationSource) {
        if (INSTANCE == null) {
            INSTANCE = new LocationRepository(fusedLocationSource);
        }
        return INSTANCE;
    }

    /**
     * Used to force {@link #getInstance(LocationSource)} to create a new instance
     * next time it's called.
     */
    public static void destroyInstance() {
        INSTANCE = null;
    }

    @Override
    public void startWithCallback(@NonNull final LocationSource.LocationCallback callback)
    {
        final LocationSource.LocationCallback locationCallback = new LocationSource.LocationCallback()
        {
            @Override
            public void locationFound(Location location)
            {
                // So we don't update anything once we have an initial location
                if (!mLocationIsDirty)
                    mLocationIsDirty = true;
                else
                    return;

                mLocation = location;

                callback.locationFound(location);
            }

            @Override
            public void connected()
            {
                mFusedLocationSource.startLocationUpdates();
            }
        };

       mFusedLocationSource.setLocationCallback(locationCallback);

        mFusedLocationSource.init();
    }

    @Override
    public void setLocationCallback(LocationSource.LocationCallback locationCallback)
    {
        // Not defined
    }

    @Override
    public void startLocationUpdates()
    {
        mFusedLocationSource.startLocationUpdates();
    }

    @Override
    public void pause()
    {
        mFusedLocationSource.pause();
    }

    @Override
    public void stop()
    {
        mFusedLocationSource.stop();
    }


    @Override
    public void init()
    {

    }

    @Override
    public Location getLocation()
    {
        return mLocation;
    }
}