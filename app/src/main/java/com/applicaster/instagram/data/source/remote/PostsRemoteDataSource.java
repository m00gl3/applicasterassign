/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applicaster.instagram.data.source.remote;

import android.location.Location;
import android.support.annotation.NonNull;

import com.applicaster.instagram.Config;

import com.applicaster.instagram.data.CommentsResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.applicaster.instagram.data.PostsResponse;
import com.applicaster.instagram.data.source.PostsDataSource;

import com.applicaster.instagram.utils.PostsDeserializer;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class PostsRemoteDataSource implements PostsDataSource {
    private static final String TAG = "PostsRemoteDataSource";

    private static PostsRemoteDataSource INSTANCE;

    private Retrofit retrofit;
    private PostsAPI service;


    public static PostsRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PostsRemoteDataSource();
        }
        return INSTANCE;
    }

    // Prevent direct instantiation.
    private PostsRemoteDataSource()
    {
        initRetrofit();

        service = retrofit.create(PostsAPI.class);
    }


    private void initRetrofit()
    {
        // HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //  logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        //   httpClient.addInterceptor(logging);

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(PostsResponse.class, new PostsDeserializer())
                .create();

        retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }


    @Override
    public void getPostsByLocation(final @NonNull LoadPostsCallback callback, @NonNull Location userLocation)
    {
        // Not defined
    }

    @Override
    public void getPostsByLocation(final @NonNull LoadPostsCallback callback, @NonNull Location userLocation, @NonNull Integer distance, @NonNull String accessToken)
    {
        // Gets the posts with the API interface
        Call<PostsResponse> call = service.getPostsByLocation(userLocation.getLatitude(), userLocation.getLongitude(), distance, accessToken);

        // Async call with callback
        call.enqueue(callback);
    }

    @Override
    public void getAccessToken(final @NonNull AccessTokenCallback callback) {

        // Not defined
    }

    @Override
    public void setAccessToken(String accessToken)
    {
        // Not defined
    }

    @Override
    public void getPostComments(@NonNull String postId, @NonNull LoadCommentsCallback callback)
    {
        // Not defined
    }

    @Override
    public void getCommentsForPost(@NonNull LoadPostCommentsCallback callback, @NonNull String postId)
    {
        // Not defined
    }

    @Override
    public void getPostComments(@NonNull LoadCommentsCallback callback, @NonNull String postId, @NonNull String accessToken)
    {
        Call<CommentsResponse> call = service.getPostComments( postId, accessToken);

        // Async call with callback
        call.enqueue(callback);
    }

    @Override
    public void setDistance(int newDistance)
    {
        // Not defined
    }

}