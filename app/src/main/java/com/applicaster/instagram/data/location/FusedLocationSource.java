package com.applicaster.instagram.data.location;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.applicaster.instagram.Config;
import com.applicaster.instagram.data.source.remote.PostsRemoteDataSource;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;


import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class FusedLocationSource implements
        LocationSource,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<LocationSettingsResult> {

    protected static final String TAG = "FusedLocationSource";

    private static FusedLocationSource INSTANCE;

    private static final long INTERVAL = 1000 * 30;
    private static final long FASTEST_INTERVAL = 1000 * 5;
    private static final long ONE_MIN = 1000 * 60;
    private static final long REFRESH_TIME = ONE_MIN * 5;
    private static final float MINIMUM_ACCURACY = 50.0f;

    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    protected LocationSettingsRequest mLocationSettingsRequest;

    LocationSource.LocationCallback locationCallback;
    Context context;
    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;
    private Location location;
    private FusedLocationProviderApi fusedLocationProviderApi = LocationServices.FusedLocationApi;

    /**
     * Tracks the status of the location updates request. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    protected Boolean mRequestingLocationUpdates;

    public static FusedLocationSource getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new FusedLocationSource(context);
        }
        return INSTANCE;
    }

    private FusedLocationSource(Context context) {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);
        this.context = context;

        mRequestingLocationUpdates = false;

        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        buildLocationSettingsRequest();
    }

    @Override
    public void setLocationCallback(LocationSource.LocationCallback locationCallback) {
        this.locationCallback = locationCallback;
    }

    @Override
    public void init()
    {
        if (googleApiClient != null)
        {
            googleApiClient.connect();
        }
    }

    @Override
    public void onConnected(Bundle connectionHint)
    {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            Location currentLocation = fusedLocationProviderApi.getLastLocation(googleApiClient);
            if (currentLocation != null && currentLocation.getTime() > REFRESH_TIME) {
                location = currentLocation;

                if (locationCallback != null)
                    locationCallback.locationFound(location);

            }
            else
            {
                checkLocationSettings();
            }
        }
        else
        {
            ActivityCompat.requestPermissions((Activity)context,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    Config.MY_PERMISSIONS_REQUEST_FINE_LOCATION);
        }
    }

    @Override
    public void startWithCallback(@NonNull final LocationSource.LocationCallback callback)
    {

    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    @Override
    public void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            // If we have last location we don't need to start requesting updates
                fusedLocationProviderApi.requestLocationUpdates(googleApiClient, locationRequest, this)
                        .setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                Log.d(TAG, " mRequestingLocationUpdates = true");

                                mRequestingLocationUpdates = true;

                            }
                        });

        }
        else
        {
            return;
        }

        // Schedule a Thread to unregister location listeners
        Executors.newScheduledThreadPool(1).schedule(new Runnable() {
            @Override
            public void run() {
                fusedLocationProviderApi.removeLocationUpdates(googleApiClient,
                        FusedLocationSource.this).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                        Log.d(TAG, " mRequestingLocationUpdates = false");

                        mRequestingLocationUpdates = false;

                    }
                });
            }
        }, ONE_MIN, TimeUnit.MILLISECONDS);
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} method, with the results provided through a {@code PendingResult}.
     */
    public void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        googleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    /**
     * The callback invoked when
     * {@link com.google.android.gms.location.SettingsApi#checkLocationSettings(GoogleApiClient,
     * LocationSettingsRequest)} is called. Examines the
     * {@link com.google.android.gms.location.LocationSettingsResult} object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                Log.i(TAG, "All location settings are satisfied.");

                // Assume thisActivity is the current activity
                int permissionCheck = ContextCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_FINE_LOCATION);

                if (permissionCheck != PackageManager.PERMISSION_GRANTED)
                {
                    ActivityCompat.requestPermissions((Activity)context,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            Config.MY_PERMISSIONS_REQUEST_FINE_LOCATION);

                }
                else
                {
                    // We have permissions and location settings are on
                    locationCallback.connected();
                }


                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to" +
                        "upgrade location settings ");

                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult((Activity)context, Config.REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //if the existing location is empty or
        //the current location accuracy is greater than existing accuracy
        //then store the current location
        if (null == this.location || location.getAccuracy() < this.location.getAccuracy()) {
            this.location = location;

            if (locationCallback != null)
                locationCallback.locationFound(location);

            //if the accuracy is not better, remove all location updates for this listener
            if (this.location.getAccuracy() < MINIMUM_ACCURACY) {
                fusedLocationProviderApi.removeLocationUpdates(googleApiClient, this);
            }
        }
    }

    @Override
    public void pause()
    {
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (googleApiClient.isConnected()) {
            stop();
        }
    }
    /**
     * Removes location updates from the FusedLocationApi.
     */
    @Override
    public void stop() {

        if (googleApiClient == null)
            return;

        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        if (mRequestingLocationUpdates/* && googleApiClient.isConnected()*/)
        {
                try
                {
                    LocationServices.FusedLocationApi.removeLocationUpdates(
                            googleApiClient,
                            this
                    ).setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            Log.d(TAG, "LocationServices.FusedLocationApi.removeLocationUpdates");

                            mRequestingLocationUpdates = false;
                        }
                    });
                }
                catch (IllegalStateException ex)
                {
                    Log.d(TAG, "java.lang.IllegalStateException: GoogleApiClient is not connected yet.");
                }

        }

        // TODO: Is this needed?
        if (googleApiClient.isConnected())
        {
            Log.d(TAG, "Disconnecting googleApiClient");

            googleApiClient.disconnect();
        }


    }

    public Location getLocation() {
        return this.location;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

}