/*
* Copyright 2016, The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.applicaster.instagram.data.source;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;

import com.applicaster.instagram.data.Comment;
import com.applicaster.instagram.data.CommentsResponse;
import com.applicaster.instagram.data.Post;

import com.applicaster.instagram.data.PostsResponse;


import java.util.List;

import retrofit2.Callback;

/**
 * Main entry point for accessing Posts data.
 * <p>
 * For simplicity, only getPosts() and getPost() have callbacks. Consider adding callbacks to other
 * methods to inform the user of network/database errors or successful operations.
 * For example, when a new Post is created, it's synchronously stored in cache but usually every
 * operation on database or network should be executed in a different thread.
 */
public interface PostsDataSource {

    interface LoadPostsCallback extends Callback<PostsResponse> {

        void onPostsLoaded(List<Post> posts);

        void onDataNotAvailable();
    }

    interface LoadCommentsCallback extends Callback<CommentsResponse> {

        void onCommentsLoaded(List<Comment> comments);

        void onDataNotAvailable();
    }

    interface LoadPostCommentsCallback  {

        void onCommentsLoaded(List<Comment> comments);
    }

    interface AccessTokenCallback  {
        void accessGranted();

        void showAuthDialog();
    }

    void getPostsByLocation(@NonNull LoadPostsCallback callback, @NonNull Location userLocation);

    // Retrofit Callback
    void getPostsByLocation(@NonNull LoadPostsCallback callback, @NonNull Location userLocation, @NonNull Integer distance, @NonNull String accessToken);

    void getPostComments(@NonNull String postId, @NonNull LoadCommentsCallback callback);

    // Retrofit Callback
    void getPostComments(@NonNull LoadCommentsCallback callback, @NonNull String postId, @NonNull String accessToken);

    void getAccessToken(@NonNull AccessTokenCallback callback);

    void setAccessToken(@NonNull String accessToken);

    void getCommentsForPost(@NonNull LoadPostCommentsCallback callback, @NonNull String postId);

    void setDistance(int newDistance);


}