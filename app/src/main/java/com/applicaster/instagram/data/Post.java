package com.applicaster.instagram.data;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roy on 9/25/2017.
 */

public class Post implements Serializable
{

    @SerializedName("images")
    private Images images;

    @SerializedName("user")
    private User user;

    @SerializedName("id")
    private String id;

    @SerializedName("caption")
    private Caption caption;

    @SerializedName("comments")
    private Comments comments;

    private ArrayList<Comment> postComments;

    public Post(@Nullable String id, @Nullable User user, @Nullable Caption caption, @Nullable Comments comments, @Nullable Images images)
    {
        this.id = id;
        this.user = user;
        this.caption = caption;
        this.comments = comments;
        this.images = images;
    }

    public ArrayList<Comment> getPostComments()
    {
        if (postComments == null)
        {
            postComments = new ArrayList<>();
        }

        return postComments;
    }

  /*  public void setPostComments(List<Comment> comments) {
        this.postComments = comments;
    }*/

    public Comments getComments() {
        return comments;
    }

    public Caption getCaption() {
        return caption;
    }

    public String getId() {
        return id;
    }

    public Images getImages() {
        return images;
    }

    public User getUser() {
        return user;
    }


    static public class User implements Serializable {

        @SerializedName("profile_picture")
        private String profile_picture;

        @SerializedName("username")
        private String username;

        public String getProfile_picture() {
            return profile_picture;
        }

        public String getUserName() {
            return username;
        }

        public User(@Nullable String username, @Nullable String profile_picture)
        {
            this.username = username;
            this.profile_picture = profile_picture;
        }
    }

    static public class Caption implements Serializable {

        public String getText() {
            return text;
        }

        @SerializedName("text")
        private String text;

        public Caption(@Nullable String text)
        {
            this.text = text;
        }

    }

    static public class Comments implements Serializable {

        public int getCount() {
            return count;
        }

        @SerializedName("count")
        private int count;

        public Comments(@Nullable int count)
        {
            this.count = count;
        }

    }


    static public class Images implements Serializable {

        @SerializedName("standard_resolution")
        private Standard_resolution standard_resolution;

        public Standard_resolution getStandard_resolution() {
            return standard_resolution;
        }

        public Images(@Nullable Standard_resolution standard_resolution)
        {
            this.standard_resolution = standard_resolution;
        }

        static public class Standard_resolution implements Serializable {

            @SerializedName("url")
            private String url;

            public String getUrl() {
                return url;
            }

            public Standard_resolution(@Nullable String url)
            {
                this.url = url;
            }
        }
    }
}
