/*
* Copyright 2016, The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.applicaster.instagram.data.location;

import android.content.Context;
import android.location.Location;


import com.applicaster.instagram.data.Post;
import com.applicaster.instagram.data.PostsResponse;
import com.google.android.gms.location.LocationListener;

import java.util.List;

import retrofit2.Callback;

/**
 * Main entry point for accessing Posts data.
 * <p>
 * For simplicity, only getPosts() and getPost() have callbacks. Consider adding callbacks to other
 * methods to inform the user of network/database errors or successful operations.
 * For example, when a new Post is created, it's synchronously stored in cache but usually every
 * operation on database or network should be executed in a different thread.
 */
public interface LocationSource {

    interface LocationCallback {

        void locationFound(Location location);

        void connected();
    }

    void pause();

    void stop();

    void startLocationUpdates();

    void init();

    Location getLocation();

    void startWithCallback(final LocationCallback callback);

    void setLocationCallback(LocationSource.LocationCallback locationCallback);

}