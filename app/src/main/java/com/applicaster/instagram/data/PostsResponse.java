package com.applicaster.instagram.data;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Roy on 9/24/2017.
 */

public class PostsResponse
{
    public List<Post> getPosts() {
        return posts;
    }

    @SerializedName("data")
    private List<Post> posts;

    public PostsResponse(List<Post> posts)
    {
        this.posts = posts;
    }
}
