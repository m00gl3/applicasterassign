package com.applicaster.instagram.data.source.remote;


import com.applicaster.instagram.data.CommentsResponse;
import com.applicaster.instagram.data.PostsResponse;

import retrofit2.http.GET;
import retrofit2.Call;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by Roy on 9/24/2017.
 */
public interface PostsAPI {

    @GET("v1/media/search")
    Call<PostsResponse> getPostsByLocation(@Query("lat") Double lat, @Query("lng") Double lng, @Query("distance") Integer distance, @Query("access_token") String access_token);

    @GET("v1/media/{media-id}/comments")
    Call<CommentsResponse> getPostComments(@Path("media-id") String postId, @Query("access_token") String access_token);

}
