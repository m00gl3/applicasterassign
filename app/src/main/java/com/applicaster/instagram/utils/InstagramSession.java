package com.applicaster.instagram.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Roy on 9/25/2017.
 */

public class InstagramSession {

    private static final String SHARED = "Instagram_Preferences";

    private static final String API_ACCESS_TOKEN = "access_token";
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private Context mContext;

    public InstagramSession(Context context) {
        this.mContext = context;
        mSharedPreferences = context.getSharedPreferences(SHARED, Context.MODE_PRIVATE);

        if (mSharedPreferences != null)
            mEditor = mSharedPreferences.edit();
    }

    public InstagramSession() {
        mSharedPreferences = mContext.getSharedPreferences(SHARED, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public void storeAccessToken(String accessToken) {

        mEditor.putString(API_ACCESS_TOKEN, accessToken);

        mEditor.commit();
    }


    public void resetAccessToken() {

        mEditor.putString(API_ACCESS_TOKEN, null);

        mEditor.commit();
    }

    /**
     * Get access token
     *
     * @return Access token
     */
    public String getAccessToken()
    {
        if (mSharedPreferences == null)
        {
            return "";
        }
        else
        {
            return mSharedPreferences.getString(API_ACCESS_TOKEN, null);
        }
    }
}