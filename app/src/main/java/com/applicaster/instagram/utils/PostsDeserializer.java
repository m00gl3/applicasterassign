package com.applicaster.instagram.utils;

import com.applicaster.instagram.data.Post;
import com.applicaster.instagram.data.PostsResponse;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;


import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Roy on 9/12/2017.
 */

public class PostsDeserializer implements JsonDeserializer<PostsResponse>
{
    public PostsDeserializer()
    {

    }

    @Override
    public PostsResponse deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
            throws JsonParseException
    {

        PostsResponse postsResponse = new Gson().fromJson(je, PostsResponse.class);

        return postsResponse;

    }
}
