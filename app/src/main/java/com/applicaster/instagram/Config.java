package com.applicaster.instagram;

/**
 * Created by Roy on 9/24/2017.
 */

public class Config {
    // CHANGE THESE WITH YOUR OWN
    public static final String INSTAGRAM_CLIENT_ID = "REPLACE_WITH_YOUR_APP_CLIENT_ID";
    public static final String INSTAGRAM_REDIRECT_URI = "REPLACE_WITH_YOUR_APP_REDIRECT_URI";

    /**
     * Constant used in the location settings dialog.
     */
    public static final int REQUEST_CHECK_SETTINGS = 0x1;

    public static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 66;


    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    public static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;

    public static final String BASE_URL = "https://api.instagram.com/";

    public static final int DEFAULT_DISTANCE = 500;
    public static final int MAX_DISTANCE = 5000;
    public static final int INCREMENT_STEP_METERS = 500;

    public static final int POSTS_LIMIT = 10;




}
