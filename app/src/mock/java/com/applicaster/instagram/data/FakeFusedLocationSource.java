/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applicaster.instagram.data;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;

import com.applicaster.instagram.data.location.FusedLocationSource;
import com.applicaster.instagram.data.location.LocationSource;
import com.applicaster.instagram.data.source.PostsDataSource;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of a remote data source with static access to the data for easy testing.
 */
public class FakeFusedLocationSource implements LocationSource {

    private static FakeFusedLocationSource INSTANCE;

    private static final String PROVIDER = "flp";
    private static final double LAT = 32.083942;
    private static final double LNG = 34.786132;
    private static final float ACCURACY = 3.0f;

    private final static Location MOCK_LOCATION = createLocation(LAT, LNG, ACCURACY);

    LocationSource.LocationCallback locationCallback;

    /*
 * From input arguments, create a single Location with provider set to
 * "flp"
 */
    static private Location createLocation(double lat, double lng, float accuracy) {
        // Create a new Location
        Location newLocation = new Location(PROVIDER);
        newLocation.setLatitude(lat);
        newLocation.setLongitude(lng);
        newLocation.setAccuracy(accuracy);
        return newLocation;
    }

    Context context;

    // Prevent direct instantiation.
    private FakeFusedLocationSource(Context context)
    {
        this.context = context;

    }

    public static FakeFusedLocationSource getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new FakeFusedLocationSource(context);
        }
        return INSTANCE;
    }


    @Override
    public void pause()
    {

    }

    @Override
    public void stop()
    {

    }

    @Override
    public void startLocationUpdates()
    {
        if (locationCallback != null)
        {
            locationCallback.locationFound(MOCK_LOCATION);
        }
    }

    @Override
    public void init()
    {
        if (locationCallback != null)
        {
            locationCallback.connected();
        }
    }

    @Override
    public void startWithCallback(final LocationCallback callback)
    {
        callback.locationFound(MOCK_LOCATION);
    }

    @Override
    public Location getLocation()
    {
        return MOCK_LOCATION;
    }

    @Override
    public void setLocationCallback(LocationSource.LocationCallback locationCallback) {
         this.locationCallback = locationCallback;
    }
}
