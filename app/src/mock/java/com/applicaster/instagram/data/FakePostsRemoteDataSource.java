/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.applicaster.instagram.data;

import android.location.Location;
import android.support.annotation.NonNull;

import com.google.common.collect.Lists;
import com.applicaster.instagram.data.source.PostsDataSource;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implementation of a remote data source with static access to the data for easy testing.
 */
public class FakePostsRemoteDataSource implements PostsDataSource {

    private static FakePostsRemoteDataSource INSTANCE;

    private final static List<Post> POSTS_SERVICE_DATA;
    private final static List<Comment> COMMENTS_SERVICE_DATA;

    static {
        POSTS_SERVICE_DATA = new ArrayList<>();
        COMMENTS_SERVICE_DATA = new ArrayList<>();

        Post.User newUser = new Post.User("m00gl3","https://scontent.cdninstagram.com/t51.2885-19/s150x150/12331677_189700044711109_254624907_a.jpg");
        Comment.From newFrom = new Comment.From("m00gl3","https://scontent.cdninstagram.com/t51.2885-19/s150x150/12331677_189700044711109_254624907_a.jpg");

        addPost("1590040276411618342_14367861", newUser, new Post.Caption("test 1"), new Post.Comments(1), new Post.Images(new Post.Images.Standard_resolution("https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/21042413_2005564626338600_1836362672413605888_n.jpg")));
        addPost("1571078786493316324_14367861", newUser, new Post.Caption("test 2"), new Post.Comments(0), new Post.Images(new Post.Images.Standard_resolution("https://scontent.cdninstagram.com/t51.2885-15/sh0.08/e35/p640x640/20482647_262757217545888_4469836279571283968_n.jpg")));
        addPost("1548629929578924004_14367861", newUser, new Post.Caption("test 3"), new Post.Comments(2), new Post.Images(new Post.Images.Standard_resolution("https://scontent.cdninstagram.com/t51.2885-15/e35/19534380_240265496464493_8133223459258892288_n.jpg")));

        addComment(newFrom, "test 1");
        addComment(newFrom, "test 3");
    }

    // Prevent direct instantiation.
    private FakePostsRemoteDataSource() {
    }

    public static FakePostsRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FakePostsRemoteDataSource();
        }
        return INSTANCE;
    }

    private static void addPost(String id, Post.User user, Post.Caption caption, Post.Comments comments, Post.Images images) {
        Post newPost = new Post(id, user, caption, comments, images);
        POSTS_SERVICE_DATA.add(newPost);
    }

    private static void addComment(Comment.From from, String text)
    {
        Comment newComment = new Comment(from, text);
        COMMENTS_SERVICE_DATA.add(newComment);
    }

    @Override
    public void getPostsByLocation(@NonNull LoadPostsCallback callback, @NonNull Location userLocation) {

        // Not Defined
    }

    @Override
    public void getPostsByLocation(@NonNull LoadPostsCallback callback, @NonNull Location userLocation, @NonNull Integer distance, @NonNull String accessToken)
    {
        callback.onPostsLoaded(POSTS_SERVICE_DATA);
    }

    @Override
    public void getAccessToken(@NonNull AccessTokenCallback callback)
    {
        // Not Defined
    }

    @Override
    public void setAccessToken(String accessToken)
    {
        // Not Defined
    }

    @Override
    public void getPostComments(@NonNull String postId, @NonNull LoadCommentsCallback callback )
    {
        // Not Defined
    }

    @Override
    public void getPostComments(@NonNull LoadCommentsCallback callback, @NonNull String postId, @NonNull String accessToken)
    {
        callback.onCommentsLoaded(COMMENTS_SERVICE_DATA);
    }

    @Override
    public void getCommentsForPost(@NonNull LoadPostCommentsCallback callback, @NonNull String postId)
    {
        // Not defined
    }

    @Override
    public void setDistance(int newDistance)
    {
        // Not defined
    }
}
