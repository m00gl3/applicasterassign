An application that shows 10 media posts near a user's current location.

@ I developed the application in a MVP architecture relying on Google Samples Android Architecture for the boilerplate

This way the View is disconnected from the Model with the Presenter as the middle-man.

@ You will need to update the CLIENT_ID and REDIRECT_URI in Config.java with your own app to test the application on real data (I also implemented mocking)

@ Because of the sandbox restrictions the application is very limited and even with "public_content" scope set it only showed my posts and not much else...

@ I wasn't sure from the assignment, if you wanted to show the "post caption" or the post comments (it says "user comment").

@ Either way, I added them both. I had to find out that the "caption" field in the media object is an object in itself because the Instagram docs are not updated or show "null" on the field.

@ The comments are shown in another activity/fragment using the same MVP style.
If a post has comments, it is clickable and will move you to the next activity.
Again, because of the sandbox mode I only managed to see my own comments.

@ The initialization is sequential:
1) Check if there is a stored access_token (Instagram oAuth) under SharedPreferences
2) If there isn't one - show the authorization dialog
3) Once access is granted, check Location permissions
4) Once permissions are granted, fetch user location
5) Proceed to fetch near by posts
========================

The way that everything is organized is like this:

@ Both fragments implement a view interface and talk to a presenter which implements a presenter interface (both interfaces are defined in a contract interface)

@ The store of the application is defined in two repositories. 
One that talks to the Instagram APIs and retrieves the network data asynchronously using Retrofit and another to fetch and hold the user location (using FusedLocation)

I use Gson to deserialize the JSON into the data object.

@ Returned posts are stored in an in-memory cache which is a hash map where the key is the id of the post, and the value is the post object.

If there are comments for a post, they are also fetched.

@ API is also defined under PostsAPI (get near-by posts and get post comments).

@ The presenter is the only class that talks to the store. When data is available (through callback) it passes it back to the view via the View interface.

@ All the pieces are decoupled to allow for testing and mocking (without network for instance)

@ There are two build versions. One uses "mock" and the other "prod" for release.
This allows to test using a mock repository. 

To see the mocking in action in debug mode, inside the Injection class change the repositories to the fake mock ones. 

In release mode it uses the production injection.

@ For the location source-mocking I did not test mock location as explained here:
http://android.xsoftlab.net/training/location/location-testing.html

But just returning a mock location.

Mocking uses injection to inject the correct repositories according to the build type.

@ For Testing:

- There are unit tests in JUnit and using Mockito to mock our objects. 
These include tests for both presenters (posts and comments) as an example.